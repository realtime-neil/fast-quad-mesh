cmake_minimum_required(VERSION 3.5)
project(fast-quad-mesh)

# dpkg assumes multiarch and cmake supports it:
# https://wiki.debian.org/Multiarch/Implementation#Dynamic_debian.2F.2A_files
# https://cmake.org/cmake/help/latest/module/GNUInstallDirs.html
include(GNUInstallDirs)

set(CMAKE_CXX_STANDARD 14)

###########
## Build ##
###########

include_directories(
  inc
)

add_library(${PROJECT_NAME} SHARED
  inc/${PROJECT_NAME}/Simplify.hpp
  src/Simplify.cpp
)

set_target_properties(${PROJECT_NAME} PROPERTIES
    VERSION 0.1.0
    SOVERSION 0
)

install(DIRECTORY inc/
  DESTINATION ${CMAKE_INSTALL_FULL_INCLUDEDIR}
)

install(
  TARGETS ${PROJECT_NAME}
  LIBRARY DESTINATION ${CMAKE_INSTALL_FULL_LIBDIR}
)

install(
  FILES LICENSE.md
  DESTINATION ${CMAKE_INSTALL_FULL_DOCDIR}
)

